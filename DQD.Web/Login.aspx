﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DataQualityDashboard.Web.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <section id="loginForm">
                    <br />
                    <br />
                    <br />

                    <h3><i class="fa fa-rocket" aria-hidden="true"></i> &nbsp; &nbsp;Используйте учетную запись SalesWorks для входа.</h3>
                    <hr />
                    <asp:Panel ID="Panel_Error" runat="server" Visible="false">
                        <span class="error text-danger">
                            <asp:Label ID="Label_Error" runat="server" Text="Ошибка авторизации, укажите верный пароль и логин. Если данные введены верно обратитесь к администратору системы или в службу поддержки."></asp:Label>
                        </span>
                    </asp:Panel>
                    <div class="form-group">
                        <label for="TextBox_Login" class="col-md-6 control-label">Логин</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="TextBox_Login" class="form-control" runat="server" required></asp:TextBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="TextBox_Password" class="col-md-6 control-label" style="margin-top: 20px;">Пароль</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="TextBox_Password" class="form-control" TextMode="Password" runat="server" required></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-10" style="margin-top: 20px;">
                            <asp:Button ID="Button_Login" class="btn btn-info" runat="server" Text="Выполнить вход" OnClick="Button_Login_Click" />
                        </div>
                    </div>

                    <div class="col-md-10" style="margin-top: 50px;">
                        &nbsp;
                    </div>
                </section>
            </div>
        </div>
    </div>
</asp:Content>

