﻿<%@ Page Title="Главная страница" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DataQualityDashboard.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">

        <div class="form-inline raised" style="margin-top: 0px!important;">
            <fieldset class="form-group" style="min-width: 100%">
                <h4>Укажите ТС по которым нужно получить информацию</h4>
                <hr />

                <fieldset class="form-group ">
                    <div class="input-group" style="min-width: 70%">
                        <span id="Text_SearchForCust_icon" class="input-group-addon" style="max-width: 45px;">
                            <i class="fa fa-binoculars" aria-hidden="true"></i>&nbsp;Поиск:
                        </span>
                        <input id="Text_SearchForCust" type="text" class="form-control" />
                        <span id="Text_SearchForCust_Erase" class="input-group-addon" style="max-width: 20px;">
                            <i class="fa fa-eraser" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="checkbox">
                        <label>
                            <asp:CheckBox ID="CheckBox_HideNotFound" runat="server" ClientIDMode="Static" />&nbsp;скрыть не найденные</label>
                    </div>
                    <br />
                    <small class="text-muted">Укажите фразу или ее часть для поиска. Если отмечена опция “скрыть не найденные” – из списка будут убраны ТС, не удовлетворяющие условиям поиска.</small><br />
                    <br />
                </fieldset>
                <br />
                <fieldset class="form-group">
                    <div class="checkbox form-control-label">
                        <label style="padding-bottom: 5px;">
                            <asp:CheckBox ID="CheckBox_SelectAll" runat="server" />Отметить все</label>
                    </div>
                    &nbsp;&nbsp;
                    <label style="color: #033c73;" id="unselectALL">Снять все выделенные ТС</label>
                </fieldset>


                <div id="CustIdSelector">
                    <div style="border-radius: 10px; padding: 2px; height: 150px; background-image: linear-gradient(to bottom,#5bc0de 0,#2aabd2 100%);">
                        <asp:Panel ID="Panel_CustID" runat="server" BorderWidth="0px" Height="100%" ScrollBars="Vertical" BackColor="White" Style="margin-top: 0px">
                            <asp:CheckBoxList ID="CheckBoxList_CustID" runat="server" class="table CheckBoxList_CustID" CellPadding="2" CellSpacing="2" DataSourceID="SqlDataSource_CustID" DataTextField="cust_name" DataValueField="cust_id">
                            </asp:CheckBoxList>
                            <asp:SqlDataSource ID="SqlDataSource_CustID" runat="server" OnSelecting="SqlDataSources_Selecting"></asp:SqlDataSource>
                        </asp:Panel>
                    </div>
                </div>
                <br />
                <br />
            </fieldset>
            <%--        <h4>Укажите параметры проверки данных:</h4>--%>
            <div class="bs-callout bs-callout-info">
                <table style="width: 100%;" class="table table-striped">
                    <tr>
                        <td style="width: 25%;"><b>Укажите период проверки</b><br />
                            <asp:CheckBox ID="CheckBox_SaveDateRange" runat="server" ClientIDMode="Static" />&nbsp;запомнить
                        </td>
                        <td style="width: 25%;">
                            <asp:SqlDataSource ID="SqlDataSource_tblReportPeriods" runat="server" OnSelecting="SqlDataSources_Selecting"></asp:SqlDataSource>
                            <asp:DropDownList ID="DropDownList_SelecttblReportPeriods" runat="server" class="form-control" name="SelectReportPeriod"></asp:DropDownList>
                        </td>
                        <td style="width: 5%;">&nbsp;&nbsp;-&nbsp;даты:&nbsp;c&nbsp;</td>
                        <td style="width: 20%;">
                            <div class="form-group">
                                <div class="input-group date date_picker" id="datetimepicker1">
                                    <asp:TextBox type="text" ID="TextBox_DateFrom" class="form-control" runat="server" Style="width: 100px !important"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="glyphicon-calendar glyphicon"></span>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td style="width: 5%;">по</td>
                        <td style="width: 20%;">
                            <div class="form-group">
                                <div class="input-group date date_picker" id="datetimepicker2">
                                    <asp:TextBox type="text" ID="TextBox_DateTo" class="form-control" runat="server" Style="width: 100px !important"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="glyphicon-calendar glyphicon"></span>
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <p>
                <asp:Button ID="Button_Check" class="btn btn-info" runat="server" Text="Сформировать отчет" OnClick="Button_Check_Click" />
            </p>
        </div>

    </div>

    <asp:Panel ID="Panel_Results" runat="server" Visible="False">
        <hr />
        <div style="padding-left: 10px; padding-right: 15px">

            <b>Результа проверки на <%=DateTime.Now.ToString() %> </b>
            <br />
            <br />
            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="Данная проверка не нашла ни одной записи" ID="GridView_Result" class="data-table dt-head-center dt-head-justify" runat="server" CaptionAlign="Bottom" DataSourceID="SqlDataSource_Result" EnableModelValidation="True" OnRowDataBound="GridView_Result_RowDataBound" CellPadding="2" CellSpacing="2">
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource_Result" runat="server" OnSelecting="SqlDataSources_Selecting"></asp:SqlDataSource>
            <br />
            <%--<input type="text" class="btn btn-success" onclick="tableToExcel('resultTableToExport', 'Результат сверки')" value="Сохранить как Excel">--%>
            <input type="button" class="btn btn-success" onclick="tableToExcelBlob();" value="Сохранить как Excel">

            <div id="exportable">
                <table id="resultTableToExport" border="1" cellpadding="5" style="display: none">
                </table>
            </div>
            <iframe id="txtArea1" style="display: none"></iframe>
            <script type="text/javascript">
                var detailsRootUrl = '<%=urlRoot%>Details';
                var detailsLoadUrl = '<%=urlRoot%>LoadDetails';

                function tableToExcelBlob() {
                    var blob = new Blob([document.getElementById('exportable').innerHTML], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    saveAs(blob, "DataQualityResult.xls");
                }

                function fnExcelReport() {
                    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
                    var textRange; var j = 0;
                    tab = document.getElementById('resultTableToExport'); // id of table


                    for (j = 0; j < tab.rows.length; j++) {
                        tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                        //tab_text=tab_text+"</tr>";
                    }

                    tab_text = tab_text + "</table>";
                    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");

                    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                        txtArea1.document.open("txt/html", "replace");
                        txtArea1.document.write(tab_text);
                        txtArea1.document.close();
                        txtArea1.focus();
                        sa = txtArea1.document.execCommand("SaveAs", true, "DataQualityResult.xls");
                    }
                    else                 //other browser not tested on IE 11
                        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));


                    return (sa);
                }

                var tableToExcel = (function () {
                    var uri = 'data:application/vnd.ms-excel;base64,'
                        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
                    return function (table, name) {
                        if (!table.nodeType) table = document.getElementById(table)
                        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                        window.location.href = uri + base64(format(template, ctx))
                    }
                })()
            </script>
        </div>
    </asp:Panel>



    <!-- Modal -->
    <div id="modal-details" class="modal fade" role="dialog" tabindex='-1'>
        <div class="modal-dialog modal-dialog-full modal-lg">
            <!-- Modal content-->
            <div class="modal-content modal-content-full">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Детали проверки</h4>
                    <h6 style="color:black">Что бы закрыть это окно используйте кнопку esc или кнопку “закрыть” в нижнем правом углу.
                    <% if (Request.Browser.Browser.ToString().ToLower().IndexOf("firefox") > -1) { %>
                    Если Вы используете браузер FireFox и детали в данном окне не видны, установите <a href="https://addons.mozilla.org/en-US/firefox/addon/ignore-x-frame-options/" target="_blank">данное расширение</a>. 
                    <% } %>
                    </h6>
                </div>
                <div class="modal-body">
                    <iframe id="modal-details-frame" style="min-height: 400px; min-width: 100%" src="<%=urlRoot%>LoadDetails"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>





</asp:Content>
