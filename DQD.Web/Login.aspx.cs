﻿using DataQualityDashboard.Routines;
using System;

namespace DataQualityDashboard.Web
{
    public partial class Login : ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsUserAuthenticated())
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void Button_Login_Click(object sender, EventArgs e)
        {
            DBUser user = dbRoutines.CheckLogin(TextBox_Login.Text, TextBox_Password.Text);
            if (user != null)
            {
                SetAuthorithation(user.Login, user.User_ID);
                if (Request.Params["returl"] != null && Request.Params["returl"].Length > 2)
                {
                    Response.Redirect(Server.UrlDecode(Request.Params["returl"]));
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Panel_Error.Visible = true;
            }
        }
    }
}