﻿using DataQualityDashboard.Routines;
using System;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataQualityDashboard.Web
{
    public class ParentPage : System.Web.UI.Page
    {
        protected string connectionString = WebConfigurationManager.AppSettings["connectionString"];
        protected string jsonFileName = HttpRuntime.AppDomainAppPath + @"\jsonSql\" + WebConfigurationManager.AppSettings["jsonFile"];
        protected DBRoutines dbRoutines;

        public ParentPage() : base()
        {
            Logger.InitLogger();
            dbRoutines = new DBRoutines(connectionString, jsonFileName);
        }

        //private GetdbRoutines()
        //{

        //}
        public void ResetAuthorithation()
        {
            Session["login"] = null;
            Session["password"] = null;
            Session["user_id"] = null;
            FormsAuthentication.SignOut();
        }

        protected void SetAuthorithation(string login, int User_id)
        {
            FormsAuthentication.SetAuthCookie(login, true);
            Session["user_id"] = User_id;
            Session["login"] = login;
            string guid = Guid.NewGuid().ToString();
            Session["AuthToken"] = guid;
            Response.Cookies.Add(new HttpCookie("AuthToken", guid));
        }

        private bool CheckSessionsAuthValue()
        {
            if (Session["user_id"] == null || Session["login"] == null)
            {
                ResetAuthorithation();
                return false;
            }
            if (Request.IsAuthenticated && !Session["AuthToken"].ToString().Equals(Request.Cookies["AuthToken"].Value))
            {
                Response.ClearContent();
                Response.StatusCode = 401;
                Response.End();
                ResetAuthorithation();
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool IsUserAuthenticated()
        {
            return (CheckSessionsAuthValue() && Request.IsAuthenticated);
        }

        protected void SqlDataSources_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.CommandTimeout = 1200;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }

        protected void GridToExcell(GridView grid, String filename)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter sWriter = new StringWriter();
                HtmlTextWriter hWriter = new HtmlTextWriter(sWriter);
                grid.RenderControl(hWriter);
                Response.Output.Write(sWriter.ToString());
                Response.Flush();
                Response.End();
            }
            catch 
            {
                Response.Flush();
                Response.End();
            }

        }
    }
}