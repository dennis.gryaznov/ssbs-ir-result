﻿<%@ Page Title="Детали проверки" Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="DataQualityDashboard.Web.Details" %>
<!DOCTYPE html>
<html lang="ru">
<head runat="server" />
    <webopt:BundleReference runat="server" Path="~/Content/css" />
</head>
<body>
    <asp:Panel ID="Panel_Details" Style="padding: 10px;" runat="server" BorderColor="White" Visible="False">
        <p><%=SolutionText %></p>
        <form id="MainForm" role="form" runat="server">
            <asp:GridView ShowHeaderWhenEmpty="True" EmptyDataText="Данная проверка не нашла ни одной записи" ID="GridView_Details" class="table table-hover data-table dt-head-center dt-head-justify" runat="server" CaptionAlign="Bottom" DataSourceID="SqlDataSource_Details" EnableModelValidation="True" CellPadding="2" CellSpacing="2" OnRowDataBound="GridView_Details_RowDataBound">
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource_Details" runat="server" OnSelecting="SqlDataSources_Selecting"></asp:SqlDataSource>
            <br />
            <asp:Button ID="ButtonExcel" runat="server" OnClick="Button_Excel_Click" Text="Экспортировать в Excel" class="btn btn-success" />
        </form>
    </asp:Panel>
</body>
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>
        <%: Scripts.Render("~/bundles/detailsapplication") %>
<script type="text/javascript">
    var detailsRootUrl = '<%=urlRoot%>Details';
    var detailsLoadUrl = '<%=urlRoot%>LoadDetails';
</script>
</html>
