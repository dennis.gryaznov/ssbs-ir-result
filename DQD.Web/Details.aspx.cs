﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using DataQualityDashboard.Routines;

namespace DataQualityDashboard.Web
{
    public partial class Details : ParentPage
    {
        private string solutionText = string.Empty;
        private int historCellIndex = -1;
        protected string urlRoot;
        public string SolutionText
        {
            get { return solutionText; }
        }

        protected void Button_Excel_Click(object sender, EventArgs e)
        {
            GridToExcell(GridView_Details, (sender as Button).ToolTip);
        }


        protected void GridView_Details_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.TableSection = TableRowSection.TableHeader;
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    if (e.Row.Cells[i].Text.ToLower().Equals(JsonSqlHelper.FIELD_HISTORYKEY))
                    {
                        e.Row.Cells[i].Visible = false;
                        historCellIndex = i;
                    }
                } 
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    if (historCellIndex > 0 && historCellIndex == i)
                    {
                        e.Row.Cells[i].Visible = false;
                    }
                    else
                    {
                        e.Row.Cells[i].Text = Context.Server.HtmlDecode(e.Row.Cells[i].Text);
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            urlRoot = System.Web.VirtualPathUtility.ToAbsolute("~/");
            if (!IsUserAuthenticated())
            {
                Response.Redirect("~/Login?returl=Details" + Server.UrlEncode(HttpContext.Current.Request.Url.Query));
            }

            if (!IsPostBack)
            {

                if (Request.Params["cust_id"] == null || Request.Params["check"] == null)
                {
                    Response.Clear();
                    Response.End();
                    return;
                }

                int cust_id = -100;
                bool iscust_idvalid = int.TryParse(Request.Params["cust_id"].Trim(), out cust_id);
                string checkName = Request.Params["check"].Trim();


                if (!Request.IsAuthenticated || !iscust_idvalid || checkName.Equals(string.Empty))
                {
                    Response.Clear();
                    Response.End();
                    return;
                }

                if (!dbRoutines.IsUserHavePermission(int.Parse(Session["user_id"].ToString()), cust_id))
                {
                    Response.Clear();
                    Response.End();
                    return;
                }

                JsonSql checkDetail = dbRoutines.JsonHelper.GetJsonSqlObject(checkName);
                if (checkDetail == null)
                {
                    Response.Clear();
                    Response.Write("Проверка не найдена, или у Вас нет прав на просмотр ее деталей.");
                    Response.End();
                    return;
                }

                Panel_Details.Visible = true;
                try
                {
                    solutionText = checkDetail.SolutionText;
                    SqlDataSource_Details.ConnectionString = dbRoutines.ConnectionString;
                    SqlDataSource_Details.SelectCommand = checkDetail.Sql;
                    SqlDataSource_Details.SelectParameters.Clear();
                    SqlDataSource_Details.SelectParameters.Add("user_id", System.Data.DbType.Int32, Session["user_id"].ToString());
                    var qsArray = Request.QueryString.AllKeys
                        .Select(key => new { Name = key.ToString(), Value = Request.QueryString[key.ToString()] })
                        .ToArray();

                    for (int i = 0; i < qsArray.Length; i++)
                    {
                        SqlDataSource_Details.SelectParameters.Add(qsArray[i].Name, qsArray[i].Value);
                    }
                    GridView_Details.DataBind();
                    Title = (checkDetail.Title == null ? "Детали проверки" : checkDetail.Title);
                    ButtonExcel.ToolTip = Title;
                }
                catch (Exception ex)
                {
                    Logger.Log.Error(ex.Message);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        protected void GridView_Details_DataBound(object sender, EventArgs e)
        {
        }
    }
}