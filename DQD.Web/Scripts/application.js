﻿var cust_LimitationCount = 0;

jQuery.expr[":"].contains = function (elem, i, match, array) {
    return (elem.textContent || elem.innerText || jQuery.text(elem) || "").toLowerCase().indexOf(match[3].toLowerCase()) >= 0;
}

var waitingDialog = waitingDialog || (function ($) {
    'use strict';
    var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
			'</div>' +
		'</div></div></div>');

    return {
        show: function (message, options) {
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null 
            }, options);
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            $dialog.modal();
        },
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);


$(document).ready(function () {

    $("#unselectALL").click(function (event) {
        $('#MainContent_CheckBoxList_CustID').find('input:checkbox').prop('checked', false);
        $('#MainContent_CheckBox_SelectAll').prop('checked', false);
    });

    $(document).on("submit", "form", function (e) {
        var totalChecked = $('#MainContent_CheckBoxList_CustID').find('input:checked').length;
        if (cust_LimitationCount !== null && cust_LimitationCount > 0) {
            if (totalChecked > cust_LimitationCount) {
                e.preventDefault();
                alert('В настоящее время ограничен выбор ТС для формирования отчета! Вы можете выбрать не более ' + cust_LimitationCount + ' одновременно.');
                return false;
            } else {
                waitingDialog.show('Пожалуйста подождите, идет загрузка данных … ');
            }
        } else if ($('#MainContent_CheckBoxList_CustID').length >0 && totalChecked < 1) {
            alert("Не выбрано ни одной ТС для проверки!");
            e.preventDefault();
            return false;
        } else if ($("#MainContent_TextBox_DateFrom").val().trim().length !== 10 || $("#MainContent_TextBox_DateTo").val().trim().length !== 10) {
            alert("Укажите верно диапазон проверки данных!");
            e.preventDefault();
            return false;
        } else {
            waitingDialog.show('Пожалуйста подождите, идет загрузка данных … ');
        }
    });

    $('#MainContent_CheckBox_SelectAll').click(function () {
        $('#MainContent_CheckBoxList_CustID').find('input:checkbox:visible').prop('checked', this.checked);
    });


    $("#CheckBox_HideNotFound").change(function () {
        $('#Text_SearchForCust').keyup();
    });

    $('#Text_SearchForCust').keyup(function () {
        setFilterToCustomers($(this).val());
    });

    $('#Text_SearchForCust_Erase').click(function () {
        $('#Text_SearchForCust').val('').keyup();
    });

    function setFilterToCustomers(FindValue) {
        $('#MainContent_Panel_CustID').find('label').removeClass('cust_selected');
        $('#MainContent_Panel_CustID').find('label').each(function (index) {
            $(this).parent().parent().show();
        });

        // waves
        str = $("#MainContent_DropDownList_SelectWaves").val();
        if (!str) {
            str = "";
        }

        if (str && str.length > 0) {
            $('#MainContent_Panel_CustID').find('label').each(function (index) {
                elemetnText = $(this).first('.hidden_div').text();
                if (elemetnText.indexOf(str) < 1) {
                    $(this).parent().parent().hide();
                }
            });
            $('#MainContent_CheckBoxList_CustID').find('input:checkbox:hidden').prop('checked', false);
        }

        // BU
        str = $("#MainContent_DropDownList_SelectBU").val();
        if (!str) {
            str = "";
        }

        if (str && str.length > 0) {
            $('#MainContent_Panel_CustID').find('label').each(function (index) {
                elemetnText = $(this).first('.hidden_div').text();
                if (elemetnText.indexOf(str) < 1) {
                    $(this).parent().parent().hide();
                }
            });
            $('#MainContent_CheckBoxList_CustID').find('input:checkbox:hidden').prop('checked', false);
        }

        if ((FindValue === null) || (FindValue.length === 0)) {
            return 0;
        }
        
        var arr = FindValue.split(',');
        var size = arr.length;
        console.log(typeof arr[1]);
        if (size && size > 1 && (typeof arr[1] !== 'undefined') && arr[1] !== null) {
            $('#MainContent_Panel_CustID').find('label').each(function (index) {
                elemetnText = $(this).first('.hidden_div').text();
                var find = false;
                $.each(arr, function (index, value) {
                    if (elemetnText.indexOf(value) > -1) {
                        find = true;
                    }
                });

                if (!find) {
                    $(this).parent().parent().hide();

                } else {
                    $(this).addClass('cust_selected');
                }
            });

            $('#MainContent_CheckBoxList_CustID').find('input:checkbox:hidden').prop('checked', false);
            var container = $('#MainContent_Panel_CustID');
            var scrollTo = $('#MainContent_Panel_CustID').find('label:contains("' + arr[0] + '")').first();
            if (scrollTo.length > 0) {
                container.scrollTop(
                    scrollTo.offset().top - container.offset().top + container.scrollTop()
                );
            }
        } else {
            $('#MainContent_CheckBoxList_CustID').find('input:checkbox:hidden').prop('checked', false);
            $('#MainContent_Panel_CustID').find('label:contains("' + FindValue + '")').addClass('cust_selected');
            if ($('#CheckBox_HideNotFound').is(":checked")) {
                $('#MainContent_Panel_CustID').find('label:not(.cust_selected)').each(function (index) {
                    $(this).parent().parent().hide();
                });
                $('#MainContent_CheckBoxList_CustID').find('input:checkbox:hidden').prop('checked', false);
            }
            container = $('#MainContent_Panel_CustID');
            scrollTo = $('#MainContent_Panel_CustID').find('label:contains("' + FindValue + '")').first();
            if (scrollTo.length > 0) {
                container.scrollTop(
                    scrollTo.offset().top - container.offset().top + container.scrollTop()
                );
            }

        }
    }

    $('.realErrorSum').each(function (index) {
        var value = $(this).val();

        if (value < 30000) {
            $(this).parent().css("background-color", "green");
            $(this).parent().find('h5').css("color", "white");
            $(this).parent().find('small').css("color", "white");
        } else if (value < 100000 || value === 100000) {
            $(this).parent().css("background-color", "yellow");
            $(this).parent().find('h5').css("color", "black");
            $(this).parent().find('small').css("color", "black");

        } else if (value > 100000) {
            $(this).parent().css("background-color", "red");
            $(this).parent().find('h5').css("color", "white");
            $(this).parent().find('small').css("color", "white");
        }
    });

    $('.date_picker').datetimepicker({ locale: 'ru', format: 'DD.MM.YYYY' });

    $('.date_picker').change(function () {
        $(this).data("DateTimePicker").hide();
    });

    $("#MainContent_DropDownList_SelecttblReportPeriods").change(function () {
        var range = $(this).val();
        if (range && range !== "") {
            $("#MainContent_TextBox_DateFrom").val(range.substring(0, range.indexOf('-')));
            $("#MainContent_TextBox_DateTo").val(range.substring(range.indexOf('-') + 1));
        }
    });

    $("#MainContent_DropDownList_SelectWaves").change(function () {
        $('#MainContent_CheckBox_SelectAll').prop('checked', false);
        $('#Text_SearchForCust').keyup();
    });

    $("#MainContent_DropDownList_SelectBU").change(function () {
        $('#MainContent_CheckBox_SelectAll').prop('checked', false);
        $('#Text_SearchForCust').keyup();
    });

    $("#resultTableToExport").html($('#MainContent_GridView_Result').html());


    $('#MainContent_GridView_Result').DataTable({
        "info": false,
        "scrollY": 400,
        "scrollX": true,
        "paging": false,
        "order": [[0, "asc"]],
        columnDefs: [
            { 'orderDataType': 'dom-vat-numeric', type: 'numeric', targets: [-1, -2] }
        ],
        "pagingType": "full_numbers",
        "language": {
            "search": "Найти в таблице : ",
            "emptyTable": "Пока нет ни одной записи.",
            "lengthMenu": "Показывать по _MENU_ записей на странице",
            "paginate": {
                "first": "<<",
                "previous": "<",
                "next": ">",
                "last": ">>"
            }
        }
    });

   
    $.fn.dataTable.ext.order['dom-vat-numeric'] = function (settings, col) {
        return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
            var _value = $('input', td).val();
            var _res = (_value && _value !== 'undefined' ? _value * 1 : 0);
            return Number((_res).toFixed(0));
        });
    }

    if ($("#MainContent_Panel_Results").length) {
        $('html, body').animate({
            scrollTop: $("#MainContent_Panel_Results").offset().top
        }, 1300);
    }

    function buidDetailUrl(checkName, params) {
        if (typeof detailsRootUrl !== 'undefined') {
            return detailsRootUrl + '?check=' + checkName + '&' + params;
        }
    }

    function buidDetailUrlByHref(href) {
        if (typeof detailsRootUrl !== 'undefined') {
            return detailsRootUrl + '?' + href;
        }
    }

    function loadDetails(src) {
        $('#modal-details-frame').attr('src', src);
    }

    $('#modal-details').on('hidden.bs.modal', function () {
        if (typeof detailsLoadUrl !== 'undefined') {
            $('#modal-details-frame').attr('src', detailsLoadUrl);
        }
    });
    
    $(".error-cell , .error-cell-second").click(function () {
        var src = buidDetailUrlByHref($(this).attr('href'));
        if (src && src.length > 0) {
            $('#modal-details').modal({ keyboard: true });
            setTimeout(loadDetails, 500, src);
        } 
    });

    $(window).resize(function () {
        var height = ($('#modal-details').height() - 190) + 'px';
        $('#modal-details-frame').css('min-height', height);
    });

    $(window).resize();

    $('#Text_SearchForCust').keyup();

    function ping() {
        if (typeof detailsRootUrl !== 'undefined') {
            $.ajax({
                url: detailsRootUrl,
                success: function (result) {
                    if (result && result !== '') {
                        console.log(result);
                    }
                },
                error: function (result) {
                    if (result && result !== '') {
                        console.log(result);
                    }
                }
            });
        }
    }

    setInterval( ping, 1000 * 60 );
});


