﻿$(document).ready(function () {

    function buidDetailUrlByHref(href) {
        if (typeof detailsRootUrl !== 'undefined') {
            return detailsRootUrl + '?' + href;
        }
    }

    $(".error-cell , .error-cell-second").click(function () {
        var src = buidDetailUrlByHref($(this).attr('href'));
        if (src && src.length > 0) {
            var win = window.open(src, '_blank');
            win.focus();
        }
    });

});