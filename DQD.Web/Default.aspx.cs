﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataQualityDashboard.Configuration;
using DataQualityDashboard.Routines;

namespace DataQualityDashboard.Web
{
    public partial class _Default : ParentPage
    {
        protected string urlRoot;
        protected SqlParamsElementCollection sqlparams;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlparams = SqlParamsRetriever.GetParams();
            if (!IsUserAuthenticated())
            {
                Response.Redirect("~/Login");
            }
            if (!IsPostBack)
            {
                DataBinds();
                LoadCookies();
            }

            urlRoot = System.Web.VirtualPathUtility.ToAbsolute("~/");
        }


        #region Data binds
        private void DataBinds()
        {
            DataBind_CheckBoxList_CustID();
            DataBind_DropDownList_ReportPeriods();
        }

        private void DataBind_GridView_Result(string cust_ids)
        {
            try
            {
                SqlDataSource_Result.ConnectionString = null;
                SqlDataSource_Result.DataSourceMode = SqlDataSourceMode.DataReader;
                SqlDataSource_Result.EnableCaching = false;
                SqlDataSource_Result.ConnectionString = dbRoutines.ConnectionString;
                SqlDataSource_Result.SelectCommand = dbRoutines.JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_DASHBOARD_WEB);
                SqlDataSource_Result.SelectParameters.Clear();
                SqlDataSource_Result.SelectParameters.Add("d1", TextBox_DateFrom.Text.Trim());
                SqlDataSource_Result.SelectParameters.Add("d2", TextBox_DateTo.Text.Trim());
                SqlDataSource_Result.SelectParameters.Add("cust_ids", cust_ids.Trim());
                if (sqlparams != null && sqlparams.Count > 0)
                {
                    foreach(SqlParamsElement element in sqlparams)
                    {
                        SqlDataSource_Result.SelectParameters.Add(element.Name, (System.Data.DbType) Enum.Parse(typeof(System.Data.DbType), element.Type), element.Value);
                    }
                }
                GridView_Result.DataBind();
                foreach (DataControlField field in GridView_Result.Columns)
                {
                    if (field.HeaderText.ToLower().Equals(JsonSqlHelper.FIELD_HISTORYKEY))
                    {
                        field.Visible = false;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message);
            }
        }

        private void DataBind_CheckBoxList_CustID()
        {
            try
            {
                SqlDataSource_CustID.ConnectionString = dbRoutines.ConnectionString;
                SqlDataSource_CustID.SelectCommand = dbRoutines.JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_CUSTOMERS);
                SqlDataSource_CustID.SelectParameters.Clear();
                SqlDataSource_CustID.SelectParameters.Add("user_id", System.Data.DbType.Int32, Session["user_id"].ToString());
                if (sqlparams != null && sqlparams.Count > 0)
                {
                    foreach (SqlParamsElement element in sqlparams)
                    {
                        SqlDataSource_CustID.SelectParameters.Add(element.Name, (System.Data.DbType)Enum.Parse(typeof(System.Data.DbType), element.Type), element.Value);
                    }
                }
                CheckBoxList_CustID.DataBind();
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message);
            }
        }

        private void DataBind_DropDownList_ReportPeriods()
        {
            try
            {
                SqlDataSource_tblReportPeriods.ConnectionString = dbRoutines.ConnectionString;
                SqlDataSource_tblReportPeriods.SelectCommand = dbRoutines.JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_PERIODS);
                if (sqlparams != null && sqlparams.Count > 0)
                {
                    foreach (SqlParamsElement element in sqlparams)
                    {
                        SqlDataSource_tblReportPeriods.SelectParameters.Add(element.Name, (System.Data.DbType)Enum.Parse(typeof(System.Data.DbType), element.Type), element.Value);
                    }
                }
                SqlDataSource_tblReportPeriods.DataBind();

                DropDownList_SelecttblReportPeriods.DataSource = SqlDataSource_tblReportPeriods;
                DropDownList_SelecttblReportPeriods.DataValueField = "range";
                DropDownList_SelecttblReportPeriods.DataTextField = "title";
                DropDownList_SelecttblReportPeriods.DataBind();

                DropDownList_SelecttblReportPeriods.Items.Insert(0, new ListItem(String.Empty, String.Empty));
                DropDownList_SelecttblReportPeriods.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message);
                Logger.Log.Error("Возможно в выборке нет колонок с именами range и title. По ним выбираются значения в выпадайки. В range должны быть два диапазона дат через дефис, например 11.02.2020 – 23.03.3030");
            }
        }
        #endregion

        #region Cookies
        private void LoadCookies()
        {
            LoadCookie_CustIDs();
            LoadCookie_DateRange();
        }

        private void LoadCookie_CustIDs()
        {
            HttpCookie cookie = Request.Cookies["CheckBoxList_CustID_SelectedItems"];
            if (cookie != null && cookie.Values != null)
            {
                foreach (string key in cookie.Values.Keys)
                {
                    ListItem item = CheckBoxList_CustID.Items.FindByValue(key);
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        private void LoadCookie_DateRange()
        {
            HttpCookie cookie = Request.Cookies["CheckBox_SaveDateRange"];
            if (cookie != null && cookie.Values != null)
            {
                CheckBox_SaveDateRange.Checked = true;
                TextBox_DateFrom.Text = cookie.Values["DateFrom"];
                TextBox_DateTo.Text = cookie.Values["DateTo"];
            }

        }

        private void SaveCookie_CustIDs()
        {
            HttpCookie cookie = new HttpCookie("CheckBoxList_CustID_SelectedItems");
            cookie.Expires = DateTime.Now.AddYears(100);

            foreach (ListItem item in CheckBoxList_CustID.Items)
            {
                if (item.Selected)
                {
                    cookie.Values.Add(item.Value, "selecteditem");
                }
            }
            Response.Cookies.Add(cookie);
        }

        private void SaveCookie_DateRange()
        {
            HttpCookie cookie = new HttpCookie("CheckBox_SaveDateRange");
            if (CheckBox_SaveDateRange.Checked)
            {
                cookie.Values.Add("DateFrom", TextBox_DateFrom.Text);
                cookie.Values.Add("DateTo", TextBox_DateTo.Text);
                cookie.Expires = DateTime.Now.AddYears(100);
            }
            else
            {
                cookie.Values.Clear();
                cookie.Expires = DateTime.Now.AddDays(-1);
            }

            Response.Cookies.Add(cookie);
        }

        private void SaveCookies()
        {
            SaveCookie_CustIDs();
            SaveCookie_DateRange();
        }
        #endregion Cookies

        /// <summary>
        /// get main result button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button_Check_Click(object sender, EventArgs e)
        {
            SaveCookies();
            String cust_ids = CheckAndGetCustIDsFromRequest();

            if (cust_ids.Length < 1 || TextBox_DateFrom.Text.Trim().Equals(string.Empty) || TextBox_DateTo.Text.Trim().Equals(string.Empty))
            {
                Panel_Results.Visible = false;
            }
            else
            {
                Panel_Results.Visible = true;
                DataBind_GridView_Result(cust_ids);
            }
        }

        /// <summary>
        /// Прверка, что переданы от формы только числа, и никакой инъекции 
        /// </summary>
        /// <returns></returns>
        private string CheckAndGetCustIDsFromRequest()
        {
            String cust_ids = String.Empty;
            int n;

            foreach (ListItem item in CheckBoxList_CustID.Items)
            {
                if (item.Selected)
                {
                    // validate only for numbers
                    if (!(int.TryParse(item.Value, out n)))
                    {
                        cust_ids = String.Empty;
                        break;
                    }
                    cust_ids += ((cust_ids.Length != 0) ? "," + item.Value : item.Value);
                }
            }
            return cust_ids;
        }

        protected void GridView_Result_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // заменим что вернулось из SQL сервера на html код в шапке 
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.TableSection = TableRowSection.TableHeader;
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Context.Server.HtmlDecode(e.Row.Cells[i].Text.Replace(@"--VAT--", @"<br/><h5 style=""color:blue"">С НДС<br /><small>без НДС</small></h5>"));
                }
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int i = 0; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = Context.Server.HtmlDecode(e.Row.Cells[i].Text);
                    //проверка что есть продажи, и их колоркоддинг  
                    if (e.Row.Cells[i].Text.ToLower().IndexOf("check-zero-sum") > 0)
                    {
                        if (e.Row.Cells[i].Text.ToLower().IndexOf("value=\"0.00\"") > 0)
                        {
                            e.Row.Cells[i].Text = "0";
                            e.Row.Cells[i].ForeColor = System.Drawing.Color.White;
                            e.Row.Cells[i].BackColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
        }
    }
}