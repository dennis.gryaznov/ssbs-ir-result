﻿using System;
using System.Web.UI;

namespace DataQualityDashboard.Web
{
    public partial class SiteMaster : MasterPage
    {
        protected void LinkButton_Exit_Click(object sender, EventArgs e)
        {
            if (this.Page is ParentPage)
            {
                (this.Page as ParentPage).ResetAuthorithation();
                if (Context != null)
                {
                    Context.Session.Clear();
                    Context.Session.Abandon();
                    Context.Session.RemoveAll();

                    if (Context.Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Context.Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Context.Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Context.Request.Cookies["AuthToken"] != null)
                    {
                        Context.Response.Cookies["AuthToken"].Value = string.Empty;
                        Context.Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                }
                Response.Redirect("~/Login");
            }
        }
    }
}