﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DataQualityDashboard.Routines
{
    /// <summary>
    /// Helper for JsonSQL local list
    /// </summary>
    public class JsonSqlHelper
    {
        public const string SECTION_LOGINUSER_FORM = "Main.LoginUserForm";
        public const string SECTION_LOGINUSER_ADFS = "Main.LoginUserADFS";
        public const string SECTION_USER_CHECKPERMISION = "Main.CheckUserPermission";
        public const string SECTION_PERIODS = "Main.Periods";
        public const string SECTION_CUSTOMERS = "Main.Customers";
        public const string SECTION_DASHBOARD_WEB = "Main.WebDashboard";
        public const string SECTION_DASHBOARD_EMAIL = "Main.EmailDashboard";

        public const string FIELD_HISTORYKEY = "historykey";

        private string sourceJsonFile = string.Empty;
        private List<JsonSql> listJsonSQL;

        public JsonSqlHelper(string filename)
        {
            if (!string.IsNullOrEmpty(filename))
            {
                Logger.InitLogger();
                sourceJsonFile = filename;
                listJsonSQL = LoadJsonSQLScripts(filename);
            }
        }

        /// <summary>
        /// Загрузка скриптов
        /// </summary>
        private List<JsonSql> LoadJsonSQLScripts(string filename)
        {
            try
            {
                string json = File.ReadAllText(filename);
                return JsonConvert.DeserializeObject<List<JsonSql>>(json);
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message);
                throw;
            }
        }

        /// <summary>
        /// по имени секции вернет обьект JsonSQL
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public JsonSql GetJsonSqlObject(string sectionName)
        {
            if (listJsonSQL == null)
            {
                Logger.Log.Error("Try to get json in empty var listJsonSQL");
                throw new Exception("JsonSQL is null! Check data from json to load");
            }

            foreach (JsonSql item in listJsonSQL)
            {
                if (item.Name.ToLower().Equals(sectionName.ToLower()))
                {
                    JsonSql prepareItem = item;
                    string includeCheckName = "";
                    int index = -1;
                    while (index < prepareItem.Sql.Length)
                    {
                        index = prepareItem.Sql.IndexOf("{{", index + 1);
                        if (index > 0)
                        {
                            includeCheckName = prepareItem.Sql.Substring(
                                prepareItem.Sql.IndexOf("{{", index) + 2,
                                prepareItem.Sql.IndexOf("}}", index + 3) - prepareItem.Sql.IndexOf("{{", index) - 2);
                            prepareItem.Sql = prepareItem.Sql.Replace("{{" + includeCheckName + "}}", GeSqlScript(includeCheckName));
                            index = -1;
                        }
                        else
                        {
                            index = prepareItem.Sql.Length + 1;
                        }
                    }
                    return prepareItem;
                }
            }
            // если не нашли такой секции
            Logger.Log.Error("Try to get nonexistent section " + sectionName + " in var listJsonSQL");
            return null;
        }

        /// <summary>
        /// по имени секции вернет обьект JsonSQL
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public string GeSqlScript(string sectionName)
        {
            JsonSql obj = GetJsonSqlObject(sectionName);
            if (obj != null)
            {
                return obj.Sql;
            }
            else
            {
                throw new Exception("Section " + sectionName + " was not found in json file");
            }
        }
    }
}