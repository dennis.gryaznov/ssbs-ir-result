﻿using System.Configuration;

namespace DataQualityDashboard.Configuration
{
    public class SqlParamsElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }

        [ConfigurationProperty("type", IsRequired = false, DefaultValue = "String")]
        public string Type
        {
            get { return (string)this["type"]; }
            set { this["type"] = value; }
        }
    }

    [ConfigurationCollection(typeof(SqlParamsElement))]
    public class SqlParamsElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SqlParamsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SqlParamsElement)element).Name;
        }
    }

    public class SqlParamsRetrieverSection : ConfigurationSection
    {
        [ConfigurationProperty("sqlParams", IsDefaultCollection = true)]
        public SqlParamsElementCollection Params
        {
            get { return (SqlParamsElementCollection)this["sqlParams"]; }
            set { this["sqlParams"] = value; }
        }
    }


    public class SqlParamsRetriever
    {
        protected static SqlParamsRetrieverSection _Config =
            ConfigurationManager.GetSection("sqlParamsRetriever") as SqlParamsRetrieverSection;
        
        public static SqlParamsElementCollection GetParams()
        {
            return _Config.Params;
        }
    }
}
