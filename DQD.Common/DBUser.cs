﻿namespace DataQualityDashboard.Routines
{
    /// <summary>
    /// SWE User class
    /// </summary>
    public class DBUser
    {
        public int User_ID { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public bool IsAllowToStartJobs { get; set; }
    }
}