﻿namespace DataQualityDashboard.Routines
{
    /// <summary>
    /// Сlass that describes the verification details
    /// </summary>
    public class JsonSql
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Sql { get; set; }
        public string SolutionText { get; set; }
    }
}