﻿namespace DataQualityDashboard.Routines
{
    public static class BCryptHelper
    {
        public static string HashPassword(string input, params string[] dynamicSalt)
        {
            return BCrypt.Net.BCrypt.HashPassword(input + string.Join(".", dynamicSalt), BCrypt.Net.BCrypt.GenerateSalt());
        }

        public static bool VerifyHash(string candidate, string hash, params string[] dynamicSalt)
        {
            return BCrypt.Net.BCrypt.Verify(candidate + string.Join(".", dynamicSalt), hash);
        }
    }
}