﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DataQualityDashboard.Routines
{
    /// <summary>
    /// Implements the functions of working with the database
    /// </summary>
    public class DBRoutines
    {
        public JsonSqlHelper JsonHelper { get; }

        public string ConnectionString { get; }

        public DBRoutines(string connectionString, string jsonFileName)
        {
            ConnectionString = connectionString;
            if (TryServerConnection())
            {
                JsonHelper = new JsonSqlHelper(jsonFileName);
            }
        }

        /// <summary>
        /// проверка соединения с сервером БД
        /// </summary>
        /// <returns></returns>
        public bool TryServerConnection()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    return true;
                }
                catch (Exception e)
                {
                    Logger.Log.Error(e);
                    return false;
                }
            }
        }

        public DBUser CheckLogin(string login, string password)
        {
            DBUser user = null;
            string sql = JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_LOGINUSER_FORM);
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                    {
                        cmd.Parameters.Add(new SqlParameter("Login", login));
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string hashPasswordFromDb = reader["Passwd"].ToString();
                                if (hashPasswordFromDb.Equals(password))
                                {
                                    user = new DBUser();
                                    user.User_ID = reader.GetInt32(0);
                                    user.Login = reader["Login"].ToString();
                                    user.Name = reader["Name"].ToString();
                                }
                                else if (BCryptHelper.VerifyHash(password, hashPasswordFromDb, "SalesWorks", login.ToLowerInvariant()))
                                {
                                    user = new DBUser();
                                    user.User_ID = reader.GetInt32(0);
                                    user.Login = reader["Login"].ToString();
                                    user.Name = reader["Name"].ToString();
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.Log.Error(e.Message);
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            GC.Collect();
            return user;
        }

        /// <summary>
        /// is adfs user exists in salesworks check 
        /// </summary>
        /// <param name="ADFS_Id">adfs login</param>
        /// <returns></returns>
        public bool IsUserExists(string ADFS_Id)
        {
            bool bResult = false;
            if (!ADFS_Id.Trim().Equals(string.Empty))
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    string queryString = JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_LOGINUSER_ADFS);
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.CommandTimeout = 1200;
                    command.Parameters.Add("@ADFS_Id", SqlDbType.VarChar, 128);
                    command.Parameters["@ADFS_Id"].Value = ADFS_Id;
                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                bResult = reader.HasRows;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log.Error(e.Message);
                        bResult = false;
                    }
                }
            }
            return bResult;
        }

        /// <summary>
        /// is user have permissions to customer in salesworks check
        /// </summary>
        /// <param name="user_id">login user_id</param>
        /// <param name="cust_id">cust_id from tblCustomers</param>
        /// <returns></returns>
        public bool IsUserHavePermission(int user_id, int cust_id)
        {
            bool bResult = false;
            if (user_id > 0)
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    string queryString = JsonHelper.GeSqlScript(JsonSqlHelper.SECTION_USER_CHECKPERMISION);
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.CommandTimeout = 1200;
                    command.Parameters.Add("@user_id", SqlDbType.Int);
                    command.Parameters["@user_id"].Value = user_id;
                    command.Parameters.Add("@cust_id", SqlDbType.Int);
                    command.Parameters["@cust_id"].Value = cust_id;
                    try
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                bResult = reader.HasRows;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Log.Error(e.Message);
                        bResult = false;
                    }
                }
            }
            return bResult;
        }

    }
}