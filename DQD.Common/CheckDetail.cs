﻿namespace DataQualityDashboard.Routines
{
    /// <summary>
    /// Сlass that describes the verification details
    /// </summary>
    public class CheckDetail
    {
        public string Name { get; set; }
        public string SQLQuery { get; set; }
        public string SolutionText { get; set; }
        public bool Exists { get; set; }
    }
}